(function(exports) {
    "use strict";

    /**
     * Creates a container
     * @class      Container
     * @classdesc  Container is used to store objects, in this case Paths.
     * @param      {Object}     element     A select element in the DOM
     *
     * @property   {Object}     element     DOM element
     * @property   {Object}     slots       Save slots
     */
    function Container(element) {
        this.element = element;
        this.slots = {};
    }
    exports.Container = Container;

    Container.prototype = {
        /**
         * Saves an object to a slot defined by key
         *
         * @param      {String}  key        The key
         * @param      {Object}  value      The value
         *
         * @memberof   Container
         */
        save: function(key, value) {
            this.slots[key] = value;
        },

        /**
         * Get data from all slots packed to a single object
         *
         * @return     {Object}     The data
         *
         * @memberof   Container
         */
        getData: function() {
            var ret = {};

            for (var key in this.slots) {
                ret[key] = this.slots[key].getData();
            }

            return ret;
        }
    };
})(this);
