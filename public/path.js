const MAX_COORDINATES = 10;

(function(exports) {
    "use strict";

    /**
     * Creates a new Path with polyline, markers and heatmap
     * @class      Path
     * @classdesc  A path with polyline, markers and heatmap.
     * @param      {Object}     map         Google Maps instance
     *
     * @property   {Object}     map         Google Maps instance
     * @property   {Object}     poly        Path polyline
     * @property   {Array}      markers     Path markers
     * @property   {Object}     heatmap     Path heatmap layer
     */
    function Path(map) {
        this.map = map;
        this.poly = new google.maps.Polyline({
            strokeColor: "#000000",
            strokeOpacity: 1.0,
            strokeWeight: 3
        });
        this.poly.setMap(map);
        this.markers = [];

        this.heatmap = new google.maps.visualization.HeatmapLayer();
    }
    exports.Path = Path;

    Path.prototype = {
        /**
         * Adds a point to the path. Called by Google Maps API "click" event.
         * Mostly copy-pasted from the API documentation.
         *
         * @param      {Event}  event       The event
         *
         * @memberof   Path
         */
        addLatLng: function(event) {
            if (this.markers.length >= MAX_COORDINATES) {
                alert("Maximum amount of coordinates reached (" + MAX_COORDINATES + ")");
                return;
            }

            // Because path is an MVCArray, we can simply append a new coordinate
            // and it will automatically appear.
            var path = this.poly.getPath();
            path.push(event.latLng);

            // Add a new marker at the new plotted point on the polyline.
            var marker = new google.maps.Marker({
              position: event.latLng,
              title: "#" + path.getLength(),
              map: this.poly.getMap() ? this.map : null
            });
            this.markers.push(marker);
        },

        /**
         * Gets the Path coordinates
         *
         * @return     {Array}  The coordinates
         *
         * @memberof   Path
         */
        getData: function() {
            return this.poly.getPath().b;
        },

        /**
         * Sets the map polyline and markers are visible on
         *
         * @param      {Object}  map        Google Maps instance. Use null for hiding.
         *
         * @memberof   Path
         */
        setMap: function(map) {
            this.poly.setMap(map);

            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(map);
            }
        },

        /**
         * Shows the heatmap and hides the polyline and markers
         *
         * @memberof   Path
         */
        showHeatmap: function() {
            this.setMap(null);
            this.heatmap.setData(this.poly.getPath());
            this.heatmap.setMap(this.map);

        },

        /**
         * Hides the heatmap and shows the polyline and markers
         *
         * @memberof   Path
         */
        hideHeatmap: function() {
            this.heatmap.setMap(null);
            this.setMap(this.map);
        },

        /**
         * Hides polyline and heatmap. Hides and clears markers
         *
         * @memberof   Path
         */
        clear: function() {
            this.setMap(null);
            this.heatmap.setMap(null);
            this.markers = [];
        }

    };
})(this);