var expect = chai.expect;
var page;

function initMap() {
    page = new Page(new MyMap());
}

describe("Page", function() {
    describe("constructor", function() {
        it("should have a saved paths container", function() {
            expect(page.savedPaths).to.have.property("element");
            expect(page.savedPaths).to.have.property("slots");
            expect(page.savedPaths).to.have.property("save");
            expect(page.savedPaths).to.have.property("getData");
        });
    });
});

describe("Saved Paths Container", function() {
    describe("save", function() {
        it("should be able to save 10 paths", function() {
            var c = page.savedPaths;
            for (var i = 0; i < 10; i++) {
                c.save("Path " + i, new Path());
            }
            for (var i = 0; i < 10; i++) {
                expect(c.slots["Path " + i]).to.have.property("map");
                expect(c.slots["Path " + i]).to.have.property("poly");
                expect(c.slots["Path " + i]).to.have.property("markers");
                expect(c.slots["Path " + i]).to.have.property("heatmap");
                expect(c.slots["Path " + i]).to.have.property("addLatLng");
                expect(c.slots["Path " + i]).to.have.property("getData");
                expect(c.slots["Path " + i]).to.have.property("setMap");
                expect(c.slots["Path " + i]).to.have.property("showHeatmap");
                expect(c.slots["Path " + i]).to.have.property("hideHeatmap");
                expect(c.slots["Path " + i]).to.have.property("clear");
            }
        });
    });
});

// No time for more at the moment...
