(function(exports) {
    "use strict";

    /**
     * Creates a new Page
     *
     * @class      Page
     * @classdesc  Page handles the UI and DOM and.
     * @param      {MyMap}      map         The map
     *
     * @property   {MyMap}      map         The map
     * @property   {Container}  savedPaths  Container instance for saving paths
     */
    function Page(map) {
        this.map = map;
        this.savedPaths = new Container(document.getElementById("saved_paths"));
    }
    exports.Page = Page;

    Page.prototype = {

        /**
         * Saves a {@link Path} to a {@link Container}.
         *
         * @memberof   Page
         */
        savePath: function() {
            if (this.map.path.markers.length === 0) {
                alert ("Empty path. Not saving.");
                return;
            }
            var i = this.savedPaths.element.selectedIndex;
            var key = "Path " + (i + 1);

            this.savedPaths.element[i].innerHTML = key;
            this.savedPaths.save(key, this.map.path);
        },

        /**
         * Switches visible map layer between Path and Heatmap
         *
         * @memberof   Page
         */
        switchLayer: function() {
            if (document.getElementById("switch_layer").value === "Show Path") {
                this.map.path.hideHeatmap();
                document.getElementById("switch_layer").value = "Show Heat Map";
            } else {
                this.map.path.showHeatmap();
                document.getElementById("switch_layer").value = "Show Path";
            }
        },

        /**
         * Prepares saved paths in {@link Container} to a JSON blob and saves it to disk
         *
         * @memberof   Page
         */
        exportJSON: function() {
            var text = JSON.stringify(this.savedPaths.getData());
            var blob = new Blob([text], {type:"application/json"});
            var url = window.URL.createObjectURL(blob);
            var fileName = "paths.json";

            var link = document.createElement("a");
            link.download = fileName;
            link.innerHTML = "Download Paths";
            link.href = url;
            link.onclick = this._deleteLink;
            link.style.display = "none";
            document.body.appendChild(link);

            link.click();
        },

        /**
         * A helper method that deletes a download link created in exportJSON.
         *
         * @param      {Event}  event   The onclick event link is tied to
         *
         * @memberof   Page
         */
        _deleteLink: function(event) {
            document.body.removeChild(event.target);
        }
    };
})(this);
