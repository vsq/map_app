const KAMPPI = {lat: 60.1675, lng: 24.9311};

(function(exports) {
    "use strict";

    /**
     * Creates a map with Google Maps API
     * @class      MyMap
     * @classdesc  Contains a Google Map and a {@link Path}
     *
     * @property   {Object}     gMap        Google Maps instance
     * @property   {Path}       path        Path instance
     */
    function MyMap() {
        this.gMap = new google.maps.Map(document.getElementById("map"), {
            center: KAMPPI,
            zoom: 14,
            disableDefaultUI: true,
            disableDoubleClickZoom: true,
            draggable: false,
            scrollwheel: false
        });
        this.gMap.setClickableIcons(false);
        this.gMap.addListener("click", function(event) {
            this.path.addLatLng(event);
        }.bind(this), true);

        this.path = new Path(this.gMap);
    }
    exports.MyMap = MyMap;

    MyMap.prototype = {
        /**
         * Clear the path
         * @memberof   MyMap
         */
        clearPath: function() {
            this.path.clear();
            this.path = new Path(this.gMap);
        }

    }
})(this);
