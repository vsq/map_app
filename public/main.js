/**
 * Instantiates a {@link Page} and inits a {@link MyMap} in it. Called by Google Maps API.
 */
function initMap() {
    page = new Page(new MyMap());
}
