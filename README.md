# Software Developer Coding Task #

## What is this repository for? ##

* Web app showcasing parts of the Google Maps API
* Version 1.0.1

## Try it out! ##
http://lowcost-env.kdpnfcbmtt.eu-central-1.elasticbeanstalk.com/

## Technologies ###
### Node.js server ###
* There is no server-side application logic
* Templating is used to avoid having to write HTML
* Application documentation is auto-generated before server start
* Libraries used:
	* Express.js web application framework
	* Pug(ex-Jade) templating engine
	* JSDoc documentation generator

### Client ###
#### Entry point ####
* main.js defines initMap that instantiates a main object page of type Page and inits a MyMap in it
* initMap is called by Google Maps API

#### Structure ####
* Page page
	* Container savedPaths
		* Object slots{"Path 1": Path path, ...}
	* MyMap map
		* google.maps.Map gMap
		* Path path
			* Array markers[google.maps.Marker, ...]
			* google.maps.Polyline poly
			* google.maps.visualization.HeatmapLayer heatmap
* Further documentation: http://lowcost-env.kdpnfcbmtt.eu-central-1.elasticbeanstalk.com/doc

## Instructions for local installation ##
* Clone the repo
* Execute:
```
#!sh
cd map_app
npm install
npm start
```
* Browse to:
	* App: http://localhost:8081/
	* Documentation: http://localhost:8081/doc

## TODO ##
* Error handling!!!
* Implement unit tests and functional tests with mocha, chai and zombie/phantom

## Author ##
* Vesa Norrbacka
* vesa@norrbacka.fi
* +358 40 742 9579