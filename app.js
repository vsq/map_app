var express = require("express");
var app = express();
var path = require("path");

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static("public"));

/* routes */
app.get("/", function(req, res) {
	res.render("index");
});

app.get("/doc", function(req, res) {
	res.render("doc");
})

app.listen(8081);
console.log("Server started");
